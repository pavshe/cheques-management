import React from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import SaveIcon from '@material-ui/icons/Save';
const axios = require('axios').default;
const env = require("./enviroment");

const FormActions = (props) =>{
  return(
    <ButtonGroup aria-label="outlined primary button group">
    <IconButton aria-label="save" onClick={props.onSave}>
        <SaveIcon fontSize="large" />
    </IconButton>
    <IconButton aria-label="edit" onClick={props.onEdit}>
        <EditIcon fontSize="large" />
    </IconButton>
    <IconButton aria-label="delete" onClick={props.onDelete}>
        <DeleteIcon fontSize="large" />
    </IconButton>
  </ButtonGroup>
  )
}

class ClientForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        allowEdit: true,
        client: {},
      }
    }

   classTextField = {
    width: '25ch',
    margin: '8px',
    flexWrap: 'wrap',
   }
    
    componentDidMount() {
      this.updateStateWithProps();
    }

    updateStateWithProps = () => {
      this.setState({
        allowEdit: this.props.allowEdit,
        client: Object.assign({}, this.props.client),
      })
    }

    componentDidUpdate(prevProps) {
      if (prevProps.client !== this.props.client) {
        this.updateStateWithProps();
      }
    }
  
    onEdit = () => {
      this.setState({
        allowEdit: true
      })
    }

    onSave = () => {
      if (this.state.client.id) {
        return this.updateClient();
      } else {
        return this.saveClient();
      }
    }

    saveClient = () => {
      return axios.post(env.clientApi, this.state.client).then((data) => {
        this.setState({
          allowEdit: true,
          client: {},
        })
      })
    }

    updateClient = () => {

    }

    onDelete = () => {
      return axios.delete(env.clientApi, {
        params:{
          id: this.state.client.id
        }
      }).then((data) => {
        this.setState({
          allowEdit: true,
          client: {},
        })
      })
    }

    handleInputChange = (event) => {
      const target = event.target;
      const value = target.value;
      const name = target.name;
      this.setState(prevState => {
        let client = Object.assign({}, prevState.client);  
        client[name] = value;                                    
        return { client };                              
      })
    }
  
    render () {
      return (
        <div>
          <form noValidate autoComplete="off">
            <TextField style={this.classTextField} disabled= {!this.state.allowEdit} value= {this.state.client.name|| ''} name="name"  label="Name"  variant="outlined" onChange={this.handleInputChange} />
            <TextField style={this.classTextField} disabled= {!this.state.allowEdit} value= {this.state.client.brandName|| ''} name="brandName"  label="Brand Name" variant="outlined" onChange={this.handleInputChange}/>
            <TextField style={this.classTextField} disabled= {!this.state.allowEdit} value= {this.state.client.phoneNumber|| ''} name="phoneNumber" label="Phone Number" variant="outlined" onChange={this.handleInputChange}/>
            <TextField style={this.classTextField} disabled= {!this.state.allowEdit} value= {this.state.client.bankAcountNumber|| ''} name="bankAcountNumber" label="Bank Account Number" variant="outlined" onChange={this.handleInputChange}/>
            <TextField style={this.classTextField} disabled= {!this.state.allowEdit} value= {this.state.client.nationalId|| ''}  name ="nationalId" label="National ID" variant="outlined" onChange={this.handleInputChange} />
            <TextField style={this.classTextField} disabled= {!this.state.allowEdit} value= {this.state.client.isSupplier|| ''}  name="isSupplier" label="Type" variant="outlined"  onChange={this.handleInputChange}  />
            <TextField fullWidth style={{ margin: '8px',flexWrap: 'wrap',}} disabled= {!this.state.allowEdit} value= {this.state.client.address|| ''}  name="address" label="Address" variant="outlined" margin="normal"  onChange={this.handleInputChange}  />
          </form>
          <FormActions onSave={this.onSave} onDelete={this.onDelete} onEdit={this.onEdit} />
        </div>
      )
    }
  }

export default ClientForm;


