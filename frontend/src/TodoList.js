import React from 'react';

const styles = {
  textAlign: 'center'
}

let id = 0;
const Todo = props => (
	<li>
		<input type= "checkbox" checked={props.todo.checked} onChange={props.onToggle}/>
		<button onClick={props.onDelete}> delete </button>
		<span> {props.todo.text} </span>
	</li>
)

class TodoList extends React.Component{
	constructor() {
		super()
		this.state = {
			todos: [],
		}
	}

	addTodo = () => {
		const text = prompt("TODO TEXT PLEASE!");
		// this.setState(prevState => ({todos: prevState.todos.push(text)}))
		this.setState({
			todos: [... this.state.todos, {id: id++, text:text,  checked: false}]
		})
	}

	deleteTodo(id) {
		this.setState({
			todos: this.state.todos.filter((todo) => todo.id !== id)
		})
	}

	toggleTodo(id) {
		this.setState({
			todos: this.state.todos.map((todo) => {
				if (todo.id !== id) {
					return todo;
				} else {
					return {
						id: todo.id,
						text: todo.text,
						checked: !todo.checked
					}
				}
			})
		})
	}

	render() {
		return (
			<div style = {styles}>
				<div> Todo Count: {this.state.todos.length}</div>
				<div> UnChecked todo count:{this.state.todos.filter((todo) => todo.checked == false).length} </div>
				<button onClick= {this.addTodo}> Add Todo</button>
				<ul>
					{this.state.todos.map(todo => 
						<Todo 
							key={todo.id} 
							onToggle= {()=> this.toggleTodo(todo.id)} 
							onDelete = {()=> this.deleteTodo(todo.id)} 
							todo ={todo} 
						/>)
					}
				</ul>
			</div>
		)
	}

}

export default TodoList;