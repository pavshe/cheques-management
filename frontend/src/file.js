import React from 'react';
import MaterialTable from 'material-table';
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Backup from "@material-ui/icons/Backup";
const axios = require('axios').default;
const env = require("./enviroment");

class FileUpload2 extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
         file: null
      }
    }
  
    onFileUpload = ({ target }) => {
      this.setState({file: target.files[0]})
      const fileReader = new FileReader();
        fileReader.readAsDataURL(target.files[0]);
        fileReader.onload = (e) => {
          var formData = new FormData();
          formData.append(props.fileKey, target.files[0]);
          console.log(target.files[0].fileName);
          axios.post(props.url, formData, {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
          })
        };
    }
  
    render() {
      return(
        <div>
            <input accept=".csv" style={{display:"none"}} id="icon-button-file" onChange={this.onFileUpload}  type="file" />
            <label htmlFor="icon-button-file">
              <IconButton color="primary" aria-label="upload file" component="span">
                <Backup />
              </IconButton>
            </label>
            <label>{this.state.file? this.state.file.name : ''}</label>
        </div>
      );
    }
  }