import React from 'react';
import MaterialTable from 'material-table';
const axios = require('axios').default;
const env = require("./enviroment");

class Transaction extends React.Component{
  columns = [
    { title: 'Id', field: 'id' },
    { title: 'Name', field: 'client' },
    { title: 'Total Amount', field: 'totalAmount' },
    { title: 'Date', field: 'submissionDate' },
    { title: 'Transaction Type', field: 'transactionType' },
  ]
	constructor() {
    super()
    this.state = {
        columns: this.columns,
          data: [],
    }
  }

    componentDidMount() {
      this.getAllTransactions();
    }

    onRowAdd = (newData) => {
        return new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              this.setState((prevState) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
        })
    }

    onRowDelete = (oldData) => {
        return new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              this.setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
        })
    }

    onRowUpdate = (newData, oldData) => {
        return new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                this.setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
        })
    }

    onRowView = (event, rowData) => {

    }

    getAllTransactions = () => {
      axios.get(env.transactionApi).then((data)=> {
        this.setState({
          columns: this.columns,
          data: data.data
        })
      })
    }
  
    render() {
      return (
          <MaterialTable
            title="Transactions"
            columns={this.state.columns}
            data={this.state.data}
            actions={[
              {
                icon: 'visibility',
                tooltip: 'view transaction details',
                onClick: (event, rowData) => this.onRowView(event, rowData)
              }
            ]}
            editable={{
              onRowAdd: (newData) => this.onRowAdd(newData),
              onRowUpdate: (newData, oldData) => this.onRowUpdate(newData, oldData),
              onRowDelete: (oldData) => this.onRowDelete(oldData),
            }}
          />
      );
    }

}

export default Transaction;


 