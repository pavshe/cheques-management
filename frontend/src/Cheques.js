import React from 'react';
import { loadCSS } from 'fg-loadcss';
import Icon from '@material-ui/core/Icon';
import { Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';

cheque = (props) => {

} 

class Cheques extends React.Component{
	constructor() {
        super()
    }

  widgets =[
      {id: 1,className: "fa fa-address-book", color:"primary", label: "Clients", routerLink:'/clients',style: {fontSize: 120}},
      {id: 3,className: "fa fa-exchange-alt", color:"primary", label: "Transactions", routerLink:'/transactions',style: {fontSize: 120}},
      {id: 4,className: "fa fa-chart-bar", color:"primary", label: "Reports", routerLink:'/reports',style: {fontSize: 120}},
  ]
  
	render() {
		return (
            <Grid container style={{paddingTop: "10%"}} justify="center" spacing={10}>
                { this.widgets.map((widget) => (
                <Grid key={widget.id} item>
                    <Widget key={widget.id} widget= {widget}/>
                </Grid>
                ))}
            </Grid>
		)
	}

}

export default Cheques;