import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import TodoList from './TodoList';
import Home from './Home'
import Routes from './routes';
ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <TodoList /> */}
    {/* <Home /> */}
    <Routes />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
