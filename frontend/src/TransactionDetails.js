import React from 'react';
import MaterialTable from 'material-table';


class TransactionDetails extends React.Component{
	constructor(props) {
        super(props)
        this.state = {
            columns: [
                { title: 'Id', field: 'id' },
                { title: 'Cheque Number', field: 'chequeNumber' },
                { title: 'Cheque Sequence', field: 'chequeSequence' },
                { title: 'From Name', field: 'fromName' },
                { title: 'To Name', field: 'toName' },
                { title: 'Bank', field: 'bank' },
                { title: 'Amount', field: 'amount' },
                { title: 'Cheque Status', field: 'chequeStatus' },
                { title: 'Due Date', field: 'dueDate' },
              ],
              data: [
                { id: '1', chequeNumber: 1234, chequeSequence: 23, fromName: 'Shery boules', toName: 'Marline monro', amount: 9898, dueDate:'20-10-2020', chequeStatus: 'collectedFromClient' },
                { id: '2', chequeNumber: 1234, chequeSequence: 23, fromName: 'Shery boules', toName: 'Marline monro', amount: 9098, dueDate:'20-10-2020', chequeStatus: 'collectedFromClient' },
                { id: '3', chequeNumber: 1234, chequeSequence: 23, fromName: 'Shery boules', toName: 'Marline monro', amount: 9898, dueDate:'20-10-2020', chequeStatus: 'collectedFromClient' },
                { id: '4', chequeNumber: 1234, chequeSequence: 23, fromName: 'Shery boules', toName: 'Marline monro', amount: 8098, dueDate:'20-10-2020', chequeStatus: 'collectedFromClient' },
              ],
        }
    }

    onRowAdd = (newData) => {
        return new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              this.setState((prevState) => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
        })
    }

    onRowDelete = (oldData) => {
        return new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              this.setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
        })
    }

    onRowUpdate = (newData, oldData) => {
        return new Promise((resolve) => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                this.setState((prevState) => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
        })
    }
  
    render() {
        return (
            <MaterialTable
              title="Cheques on this Transaction"
              columns={this.state.columns}
              data={this.state.data}
              isEditable = {false}
              editable={{
                onRowAdd: (newData) => this.onRowAdd(newData),
                onRowUpdate: (newData, oldData) => this.onRowUpdate(newData, oldData),
                onRowDelete: (oldData) => this.onRowDelete(oldData),
              }}
            />
        );
    }

}

export default TransactionDetails;


 