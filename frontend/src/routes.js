import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from "./Home";
import Bank from './Banks';
import Transaction from './Transactions'
import Clients from './Clients';

export default function Routes() {
  return (
    <Router>
      <div>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
            <Route path="/banks">
                <Bank />
            </Route>
            <Route path="/transactions">
                <Transaction />
            </Route>
            <Route path="/clients">
                <Clients />
            </Route>
            <Route path="/cheques">
                <Users />
            </Route>
            <Route path="/">
                <Home />
            </Route>
        </Switch>
      </div>
    </Router>
  );
}



function Users() {
  return <h2>Users</h2>;
}