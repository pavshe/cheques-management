import React from 'react';
import { loadCSS } from 'fg-loadcss';
import Icon from '@material-ui/core/Icon';
import { Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';


const Widget = props => {
  React.useEffect(() => {
    const node = loadCSS(
      'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
      document.querySelector('#font-awesome-css'),
    );
    return () => {
      node.parentNode.removeChild(node);
    };
  }, []);
  return (
      <div >
         <Link to={props.widget.routerLink}>
          <Icon className= {props.widget.className} color= {props.widget.color} style= {props.widget.style} />
          <br />
          <label style ={{color: "black", fontSize:"25px", fontWeight:"bold"}}> {props.widget.label}</label>
         </Link>
      </div>
  )
}


class Home extends React.Component{
	constructor() {
    super()
  }

  widgets =[
      {id: 0, className: "fa fa-university", color:"primary", label: "Banks", routerLink:'/banks', style: {fontSize: 120}},
      {id: 1,className: "fa fa-address-book", color:"primary", label: "Clients", routerLink:'/clients',style: {fontSize: 120}},
      {id: 2,className: "fa fa-money-check-alt", color:"primary", label: "Cheques", routerLink:'/cheques',style: {fontSize: 120}},
      {id: 3,className: "fa fa-exchange-alt", color:"primary", label: "Transactions", routerLink:'/transactions',style: {fontSize: 120}},
      {id: 4,className: "fa fa-chart-bar", color:"primary", label: "Reports", routerLink:'/reports',style: {fontSize: 120}},
  ]
  
	render() {
		return (
      <Grid container style={{paddingTop: "10%"}} justify="center" spacing={10}>
         { this.widgets.map((widget) => (
          <Grid key={widget.id} item>
            <Widget key={widget.id} widget= {widget}/>
          </Grid>
          ))}
      </Grid>
		)
	}

}

export default Home;


 