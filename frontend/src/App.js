import React from 'react';
import './App.css';

const styles = {
  textAlign: 'center'
}
class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    }
  }

  increaseCount = () => {
    this.setState( prevState => ({count: prevState.count + 1}));
    this.setState( prevState => ({count: prevState.count + 1}));
    // this.setState({count: this.state.count + 1});

  }

  render () {
    return (
      <div style = {styles}>
        <div> 
          <button onClick= {this.increaseCount}>Increase</button>
        </div>
        <h1> {this.state.count} </h1>
      </div>
    )
  }
}

export default App;
