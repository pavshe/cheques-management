import React from 'react';
import MaterialTable from 'material-table';
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Backup from "@material-ui/icons/Backup";
const axios = require('axios').default;
const env = require("./enviroment");


const FileUpload = (props) => {
  return(
    <div>
       <input accept=".csv" style={{display:"none"}} id="icon-button-file" onChange={props.onFileUpload}  type="file" />
        <label htmlFor="icon-button-file">
          <IconButton color="primary" aria-label="upload file" component="span">
            <Backup />
          </IconButton>
        </label>
    </div>
  );
}


class Bank extends React.Component{
    columns =  [
      { title: 'Name', field: 'name', editable: 'onAdd' },
      { title: 'Description', field: 'translation' },
    ];
    constructor() {
      super()
      this.state = {
          columns: this.columns,
            data: [],
      }
    }
  
    componentDidMount() {
      this.getAllBanks();
    }

    onRowAdd = (newData) => {
      console.log('newDataaaaaaa......', newData);
      return axios.post(env.bankApi, newData).then((respData) => {
        this.setState((prevState) => {
            const data = [...prevState.data];
            data.push(respData.data);
            return { ...prevState, data };
          });
      })
    }

    onRowDelete = (oldData) => {
      console.log('oldData', oldData);
      return axios.delete(env.bankApi, {
        params:{
          id: oldData.id
        }
      }).then((data) => {
        this.setState((prevState) => {
          const data = [...prevState.data];
          data.splice(data.indexOf(oldData), 1);
          return { ...prevState, data };
        });
      })
    }

    onRowUpdate = (newData, oldData) => {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve();
          if (oldData) {
            this.setState((prevState) => {
              const data = [...prevState.data];
              data[data.indexOf(oldData)] = newData;
              return { ...prevState, data };
            });
          }
        }, 600);
      })
    }

    onFileUpload = ({ target }) => {
      console.log('>>>>>>>>>>File Upload.......', target);
      const fileReader = new FileReader();
        fileReader.readAsDataURL(target.files[0]);
        console.log('>>>>>>>>>>File Upload.......', target.files[0].name);
        fileReader.onload = (e) => {
            var formData = new FormData();
            formData.append("pavly", target.files[0]);
            console.log(target.files[0].fileName);
            axios.post('http://localhost:8080/cm/bank/file', formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
            }).then((respData) => {
              this.setState((prevState) => {
                const data = [...prevState.data];
                data.push(...respData.data);
                return { ...prevState, data };
              });
            })
        };
    }

    getAllBanks = () => {
      axios.get(env.bankApi).then((data)=> {
        this.setState({
          columns: this.columns,
          data: data.data
        })
      })
    }
  
    render() {
        return (
            <div>
              <MaterialTable
                title="Banks"
                columns={this.state.columns}
                data={this.state.data}
                editable={{
                  onRowAdd: (newData) => this.onRowAdd(newData),
                  onRowUpdate: (newData, oldData) => this.onRowUpdate(newData, oldData),
                  onRowDelete: (oldData) => this.onRowDelete(oldData),
                }}
                actions={[
                  {
                    icon: props => ( <FileUpload onFileUpload = {(event)=> this.onFileUpload(event)}/>) ,
                    tooltip: 'Add User',
                    isFreeAction: true,
                  }
                ]}
              />
            </div>
        );
    }

    
}

export default Bank;


 