import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import MaterialTable from 'material-table';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import ClientForm from './ClientForm';
const axios = require('axios').default;
const env = require("./enviroment");

const ListStyle = {
  width: '100%',
  maxWidth: 360,
  position: 'relative',
  overflow: 'auto',
  maxHeight: 900,
}


const ClientItem = props => (
    <div>
        <ListItem alignItems="flex-start" button onClick={()=> props.onChoose(props.client)}>
          <ListItemAvatar>
            <Avatar>{props.client.brandName.charAt(0)}</Avatar>
          </ListItemAvatar>
          <ListItemText primary={props.client.brandName}
            secondary={
              <React.Fragment>
                <Typography
                  component="span"
                  variant="body2"
                  style={{display: 'inline'}}
                  color="textPrimary">
                  {props.client.name}
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider variant="inset" component="li" />
    </div>
);


const ChequeSummery = (props) => {
  return (
    <MaterialTable
      title="Cheque Summery"
      columns={[
        { title: 'Project', field: 'project' },
        { title: 'Total', field: 'total' },
        { title: 'Rejected', field: 'rejected' },
        { title: 'Collected', field: 'collected'},
      ]}
      data={props.data}        
      options={{
        sorting: true
      }}
    />
  )
}

class Clients extends React.Component {
    constructor() {
        super();
        this.state = {
            clients: [],
            displayedClients: [],
            chosenClient: {},
            allowEdit: true
        }
    }
   
    chequeSummeryData = [
      {project:"W2019", total:"50", rejected:"20", collected:"30"},
      {project:"S2019", total:"50", rejected:"20", collected:"30"},
      {project:"W2018", total:"50", rejected:"20", collected:"30"},
      {project:"S2018", total:"50", rejected:"20", collected:"30"},
      {project:"W2017", total:"50", rejected:"20", collected:"30"},
    ]

    searchFilter = (event) => {
      const filterValue = event.target.value;
      const filteredData = this.state.clients.filter((client) => {
        return client.name.toLowerCase().includes(filterValue) || client.brandName.toLowerCase().includes(filterValue)
      });
      this.setState({
        displayedClients: [...filteredData]
      })
    }

    componentDidMount() {
      this.getClients();
    }

    getClients = () => {
      axios.get(env.clientApi).then((data)=> {
        this.setState({
          clients: [...data.data],
          displayedClients: [...data.data],
          chosenClient: {}
        })
      })
    }

    onClickAdd = () => {
      this.setState({
        chosenClient: {},
        allowEdit: true
      })
    }

    onChoosingClient = (client) => {
      console.log('onChoosing Client', client);
      this.setState({
        chosenClient: Object.assign({}, client),
        allowEdit: false,
      })
    }
      
    render() {
        return (
          <div style={{flexGrow: 1}}>
            <Grid container spacing={1}>
              <Grid item xs={2}>
                  <TextField  style={ListStyle} id="outlined-basic" label="Search" onInput={this.searchFilter} variant="outlined" />
              </Grid>
              <Grid item xs={1} >
                <IconButton aria-label="add" onClick={this.onClickAdd}>
                  <AddCircleIcon fontSize="large" />
                </IconButton>
              </Grid>
              <Grid item xs={9}>
                <Avatar style={{width: '60px', height: '60px'}} />
              </Grid>
              <Grid item xs={3}>
                  <List style={ListStyle}>
                    {this.state.displayedClients.map(client => 
                      <ClientItem key= {client.id} client={client} onChoose={this.onChoosingClient}/>
                      )}
                  </List>
              </Grid>
              <Grid item xs={8}>
                  <ClientForm allowEdit={this.state.allowEdit} client={this.state.chosenClient}/>
                  <br />
                  <ChequeSummery data={this.chequeSummeryData} />
              </Grid>
            </Grid>
        </div>
        )
    }

}

export default Clients;


// render() {
//   return (
//       <div style={{display:"flex"}}>
//           <TextField  id="outlined-basic" label="Search" onInput={this.filter} variant="outlined" />
//           <List style={ListStyle}>
//               {this.state.displayedClients.map(client => 
//                   <ClientItem key= {client.id}client={client}/>
//                   )}
//           </List>
//           <ClientForm />
//       </div>
//   )
// }

 // clients = [
    //   {id:1, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098" },
    //   {id:2, brandName:"John double v", name:"Jan Iskandar", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:3, brandName:"Pavly", name:"Pavly Farag", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:4, brandName:"Mira tex", name:"Mira Fayez", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:5, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:6, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:7, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:8, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:9, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:10, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:11, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:12, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:13, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:14, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:15, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:16, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:17, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:18, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:19, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    //   {id:20, brandName:"Eman tex", name:"Atef khalifa", bankAcountNumber:"0123393098084", nationalId:"2900303980098"},
    // ]
