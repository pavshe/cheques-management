module.exports = 
[
    {
        "model": "chequeStatus",
        "data": {
            "name": "CollectedFromClient"
        }
    },
    {
        "model": "chequeStatus",
        "data": {
            "name": "ReadyForBankSubmission"
        }
    },
    {
        "model": "chequeStatus",
        "data": {
            "name": "SubmittedToBank"
        }
    },
    {
        "model": "chequeStatus",
        "data": {
            "name": "RejectedFromBank",
        }
    },
    {
        "model": "chequeStatus",
        "data": {
            "name": "ClearedDebit",
        }
    },
    {
        "model": "chequeStatus",
        "data": {
            "name": "BankCollectionSucess",
        }
    },
    {
        "model": "chequeStatus",
        "data": {
            "name": "Destroyed",
        }
    }
]
