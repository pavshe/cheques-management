const db = require("../models");
const Bank = db.bank;
const msg = require("../utils/msg")
const fileReader = require('../utils/fileReader')

var create = (bank, translation) => {
    return Bank.create({name: bank, translation: translation});
}

var bulkCreate = (banks, t) => {
    return Bank.bulkCreate(banks, {returning: true, transaction:t});
}

var findAll = () => {
    return Bank.findAll();
}

exports.findOne = (id) => {
    return Bank.findByPk(id);
}

var destroy = (id) => {
    return Bank.destroy({where:{id : id}});
}

var AddBankHelper = (req,res) => {
    if (!req.body.name) {
        return res.status(400).send({error: msg.noNameProvided})
    }
    if (!req.body.translation) {
        return res.status(400).send({error: msg.noTranslationProvided})
    }
    return create(req.body.name, req.body.translation);
}

var getAllBanksAPI = (req,res) => {
    findAll().then((data) => {
        return res.status(200).send(data);
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

var getBankByIdAPI = (req,res) => {
    if (!req.query.id) {
        return res.status(400).send({error: msg.noIdProvided})
    }
    this.findOne(req.query.id).then((data) => {
        if (data) {
            return res.status(200).send(data);
        } else {
            return res.status(404).send({message: msg.notFound}); 
        }
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

exports.addBankAPI = (req,res) => {
    AddBankHelper(req,res).then((data)=> {
        return res.status(200).send(data);
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

exports.getBankAPI = (req,res) => {
    if (!req.query.id) {
        return getAllBanksAPI(req,res);
    } else {
        return getBankByIdAPI(req, res);
    }
}

exports.deleteBankAPI = (req,res) => {
    deleteBankHelper(req,res).then((data)=>{
        return res.status(204).send();
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}


var deleteBankHelper = (req,res) => {
    if (!req.query.id) {
        return res.status(400).send({error: msg.noIdProvided})
    } else {
        return destroy(req.query.id);
    }
}



var bankValidation = (bank, res) => {
    if (!bank.name) {
        return res.status(400).send({error: msg.noNameProvided});
    }
    if (!bank.translation) {
        return res.status(400).send({error: msg.noTranslationProvided});
    }
    return bank;
}

var addBanksBulk = (banks, res, t) => {
    banks.forEach((bank) => {
        bankValidation(bank, res);
    });
    return bulkCreate(banks, t);
}

exports.addBankBulkAPI = (req,res) => {
    console.log(req.file);
    if (!req.file) {
        return res.status(400).send({error: msg.noBankProvided});
    } else {
        var buf = req.file.buffer.toString('utf-8');
        console.log('buf', buf);
        var banks = fileReader.fromCSV2JSON(buf)
        addBanksBulk(banks,res).then((data)=> {
            return res.status(200).send(data);
        }).catch((error)=> {
            return res.status(500).send({error: error})
        })
    }
}

exports.saveFileOnDisk = (req,res) => {
    console.log('...............',req.file);
    if (!req.file) {
        return res.status(400).send({error: msg.noBankProvided});
    } else {
        res.send("Success, Image uploaded!") 
    }
}