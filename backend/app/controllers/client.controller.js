
const db = require("../models");
const Client = db.client;
const msg = require("../utils/msg")

var create = (client) => {
    return Client.create(client);
}

var findAll = () => {
    return Client.findAll();
}

var findClientsOnly = () => {
    return Client.findAll({
        where: {
          isSupplier: false
        }
    });
}

var findSuppliersOnly = () => {
    return Client.findAll({
        where: {
          isSupplier: true
        }
    });
}

exports.findOne = (id) => {
    return Client.findByPk(id);
}

var destroy = (id) => {
    return Client.destroy({where:{id : id}});
}

  
var AddClientHelper = (req,res) => {
    if (!req.body.name) {
        return res.status(400).send({error: msg.noNameProvided})
    }
    if (!req.body.brandName) {
        return res.status(400).send({error: msg.noBrandNameProvided})
    }
    if (!req.body.phoneNumber) {
        return res.status(400).send({error: msg.noPhoneNumberProvided})
    }
   
    const client = {
        brandName: req.body.brandName,
        name: req.body.name,
        phoneNumber: req.body.phoneNumber,
        nationalId: req.body.nationalId,
        address: req.body.address,
        isSupplier: req.body.isSupplier ? req.body.isSupplier : false,
        bankAcountNumber: req.body.bankAcountNumber
    }
    return create(client);
}

exports.addClientAPI = (req,res) => {
    AddClientHelper(req,res).then((data)=> {
        return res.status(200).send(data);
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

var getAllClientsAPI = (req,res) => {
    getAllClientsAPIHelper(req).then((data) => {
        return res.status(200).send(data);
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

var getAllClientsAPIHelper = (req) => {
   if (req.query.isSupplier && req.query.isSupplier == 'true') {
       return findSuppliersOnly();
   } else if (req.query.isSupplier && req.query.isSupplier == 'false') {
        return findClientsOnly();
   } else {
       return findAll();
   }
}



var getClientByIdAPI = (req,res) => {
    if (!req.query.id) {
        return res.status(400).send({error: msg.noIdProvided})
    }
    this.findOne(req.query.id).then((data) => {
        if (data) {
            return res.status(200).send(data);
        } else {
            return res.status(404).send({message: msg.notFound}); 
        }
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

exports.getClientAPI = (req,res) => {
    if (!req.query.id) {
        return getAllClientsAPI(req,res);
    } else {
        return getClientByIdAPI(req, res);
    }
}

var deleteClientHelper = (req,res) => {
    if (!req.query.id) {
        return res.status(400).send({error: msg.noIdProvided})
    } else {
        return destroy(req.query.id);
    }
}

exports.deleteClientAPI = (req,res) => {
    deleteClientHelper(req,res).then((data)=>{
        return res.status(204).send();
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}







    

// // Create and Save a new user
// exports.create = (req, res) => {
//   // Validate request
//   if (!req.body.username || !req.body.first_name || !req.body.last_name) {
//     res.status(400).send({
//       message: "bad request, missing name or email"
//     });
//     return;
//   }
//   var hashedPassword = bcrypt.hashSync(req.body.password, 8);

//   // Create a User
//   const user = {
//     username: req.body.username,
//     first_name: req.body.first_name,
//     last_name: req.body.last_name,
//     token: req.body.token,
//     is_active: req.body.is_active ? req.body.is_active : false,
//     is_social: req.body.is_social ? req.body.is_social : false,
//     password: hashedPassword
//   };

  
//   // Save User in the database
//   User.create(user)
//     .then(data => {
//       result = data.toJSON();
//       delete result.password;
//       delete result.is_social;
//       delete result.is_active;

//       res.status(200).send(result);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while creating the User."
//       });
//     });
// };

// // Retrieve all users from the database.
// exports.findAllApi = (req, res) => {
//     const first_name = req.query.first_name;
//     var condition = first_name ? { first_name: { [Op.like]: `%${first_name}%` } } : null;
  
//     User.findAll({ where: condition })
//       .then(data => {
//         res.send(data);
//       })
//       .catch(err => {
//         res.status(500).send({
//           message:
//             err.message || "Some error occurred while retrieving users."
//         });
//       });
// };

// // Find a single user with an id
// exports.findOneApi = (req, res) => {
//     const username = req.params.username;
//     User.findByPk(username)
//       .then(data => {
//         if (data) {
//           result = data.toJSON();
//           delete result.password;
//           return res.send(result);
//         } else {
//           return res.status(404).send({
//             message: "NOT_FOUND"
//           });
//         }
//       })
//       .catch(err => {
//         return res.status(500).send({
//           message: "Error retrieving User with username=" + username +" , "+ err
//         });
//       });
// };

// // Update a user by the id in the request
// // exports.updateApi = (req, res) => {
// // };

// // Delete a user with the specified id in the request
// exports.deleteApi = (req, res) => {
//   User.destroy({where:{username : req.params.id}})
//   .then(data => {
//     if (data) {
//       console.log(data);
//     }
//     res.status(204).send({})
//   })
//   .catch(err => {
//     return res.status(500).send({
//       message: "Error destroying role with id=" + req.params.id + " , "+ err
//     });
//   });
// };


// // login for non social users 
// exports.loginApi = (req, res) => {
//   const username = req.body.username;
//   const missingFields = validateFieldsExist(['username', 'password'], req);
//   if (missingFields.length > 0) {
//     return res.status(400).send({
//       missingFields: missingFields,
//     });
//   } else {
//     User.findByPk(username)
//       .then(data => {
//         if (data) {
//           return loginLogic(req, data, res);
//         } else {
//           return res.status(401).send({message: "WRONG_USERNAME_OR_PASSWORD"});
//         }
//       })
//       .catch(err => {
//         // return res.status(401).send({message: "WRONG_USERNAME_OR_PASSWORD"});
//         res.status(500).send({  err: err });
//       });
//   }
// };


// // sinup return token user
// exports.signupApi = (req, res) => {
//   // Validate request
//   const missingFields = validateFieldsExist(['username', 'first_name', 'last_name', 'password'], req);
//   const inValidFields = validateFieldsByRegExp(
//     [{fieldName: 'username', regExp: validations.emailRegExp},
//     {fieldName: 'password', regExp: validations.passwordRegExp}], req);
//   if (missingFields.length > 0 || inValidFields.length > 0) {
//     return res.status(400).send({
//       missingFields: missingFields,
//       inValidFields: inValidFields
//     });
//   }

//   User.findByPk(req.body.username)
//     .then(data => {
//       if (data) {
//         return loginLogic(req,data,res);
//       } else {
//         var hashedPassword = bcrypt.hashSync(req.body.password, 8);
//         var token = jwt.sign({ 
//           username: req.body.username,
//           first_name: req.body.first_name,
//           last_name: req.body.last_name,
//           password: hashedPassword
//         }, config.secret, {
//           expiresIn: 86400 // expires in 24 hours
//         });
//         return res.status(200).send({ token: token });
//       }
//     })
//     .catch(err => {
//       return res.status(500).send({err: err})
//   });

// };

// validateFieldsExist = (arrFields, req) => {
//   var result = '';
//   for (field of arrFields) {
//     if(!req.body[field] || req.body[field] == '') {
//       result = result + field.toUpperCase() + ','
//     }
//   }
//   return result.charAt(result.length-1) == ',' ? result.slice(0, -1) : result;
// }

// // fieldsObj = [] of {fieldName: fieldName, regExp: regExp }
// validateFieldsByRegExp = (arrOfFieldsObj, req) => {
//   var result = '';
//   for (field of arrOfFieldsObj) {
//     if(!field.regExp.test(req.body[field.fieldName])) {
//       result = result + field.fieldName.toUpperCase() + ','
//     }
//   }
//   return result.charAt(result.length-1) == ',' ? result.slice(0, -1) : result;
// }


// exports.activateAccountApi = (req, res) => {
//   var bearerToken = req.headers.authorization;
//   var token = bearerToken.split(' ')[1];

//   if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  
//   jwt.verify(token, config.secret, function(err, decoded) {
//     if (err) {
//       return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
//     } else {
//       const user = {
//         username: decoded.username,
//         first_name: decoded.first_name,
//         last_name: decoded.last_name,
//         is_active: true,
//         is_social: false,
//         password: decoded.password
//       };
//       this.createUser(user, res);
//     } 
//   });
// };

// // Create and Save a new user
// createUser = (req, res) => {
//   // Create a User
//   const user = {
//     username: req.username,
//     first_name: req.first_name,
//     last_name: req.last_name,
//     password: req.password,
//     token: req.token,
//     is_active: req.is_active ? req.is_active : false,
//     is_social: req.is_social ? req.is_social : false
//   };

  

//   // Save User in the database
//   User.create(user)
//     .then(data => {
//       UserRoleController.createDB({username:req.username, role:"user"});
//       result = data.toJSON();
//       delete result.password;
//       delete result.is_social;
//       delete result.is_active;
//       return res.status(200).send(result);
//     })
//     .catch(err => {
//       return res.status(500).send({
//         message:
//           err.message || "Some error occurred while creating the User."
//       });
//     });
// };


// loginLogic = (req,userFromDB, res) => {
//   var passwordIsValid = bcrypt.compareSync(req.body.password, userFromDB.password);
//   if (passwordIsValid && userFromDB.is_active) {
//     UserRoleController.getUserRoles(userFromDB.username).then((data)=> {
//       console.log('...........................???>>>>>>>>>>>>>>>>>...........', data);
//       var token = jwt.sign({ username: userFromDB.username }, config.secret, {
//         expiresIn: 86400 // expires in 24 hours
//       });
//       var rolesArr = data.map(data => data.role);
//       User.update({ token: token }, {
//         where: {
//           username: userFromDB.username
//         }
//       }).then((data) => {
//         return res.status(200).send({ auth: true, token: token, roles: rolesArr });
//       });

//     }).catch((err)=> {
//       res.status(500).send({error: err.message || "Some error occurred while getting roles."})
//     })
    
//   } else if (!passwordIsValid) {
//     return res.status(401).send({message: "WRONG_USERNAME_OR_PASSWORD"});
//   } else { // isActive = false
//     return res.status(403).send({message: "INACTIVE_ACCOUNT"});
//   }
// };

// exports.findByPk = (username) => {
//   return User.findByPk(username);
// }




