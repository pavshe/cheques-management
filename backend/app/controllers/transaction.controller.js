const db = require("../models");
const Transaction = db.transaction;
const chequeController = require("../controllers/cheque.controller")
const clientController = require("../controllers/client.controller")
const chequeTransactionController = require("../controllers/cheque.transaction.controller")
const transactionTypeController = require("../controllers/transaction.type.controller")
const msg = require("../utils/msg")
const Op = db.Sequelize.Op
const sequelize = db.sequelize;

var create = (transaction, t) => {
    if (t) {
        console.log('transaction t........transaction');
        return Transaction.create(transaction, {transaction: t});
    } else {
        return Transaction.create(transaction);
    }
}

var findAll = () => {
    return Transaction.findAll();
}

var findOne = (id) => {
    return Transaction.findByPk(id);
}

var destroy = (id, t) => {
    if (t) {
        return Transaction.destroy({where:{transactionId : id}, transaction:t});
    } else {
        return Transaction.destroy({where:{transactionId : id}});
    }
}

var updateById = (obj, id) => {
    return Transaction.update(obj, {
        where: {
            transactionId: id
        }
    });
}

var addTransactionHelper = (transaction, req,res) => {
    return sequelize.transaction(function (t) {
        return chequeController.addBulkCheques(req.body.cheques,res, t).then((chequesResp)=> {
            console.log('FIRST................................................')
            return create(transaction, t).then((transaction)=> {
                console.log('SECOND................................................')
                let chequeTransactionArr = [];
                chequesResp.forEach((cheque) => {
                    chequeTransactionArr.push({chequeId: cheque.chequeId, transactionId: transaction.transactionId});
                })
                return chequeTransactionController.addBulkChequeTransaction(chequeTransactionArr, res, t);
            })
        })
    }).then((data)=> {
        console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', data);
        return res.status(200).send({data: data});

    }).catch((error) => {
        console.log('>>>>>>>>>>>>>>>>>>>>>>>>>error>>>>>>>>>>>>>>>>>>>>>>', error);
        return res.status(500).send({error: error});
    })
}


exports.addTransactionAPI = (req,res) => {
    if (!req.body.cheques) {
        return res.status(400).send({error: msg.noChequesProvided})
    }

    if (!req.body.clientId) {
        return res.status(400).send({error: msg.noBankProvided})
    } else {
        clientController.findOne(req.body.clientId).then((data) => {
            if (!data) {
                return res.status(400).send({error: msg.invalidClientId})
            }
        })
    }

    if (!req.body.transactionTypeId) {
        return res.status(400).send({error: msg.noBankProvided})
    } else {
        transactionTypeController.findOne(req.body.clientId).then((data) => {
            if (!data) {
                return res.status(400).send({error: msg.invalidTransactionTypeId})
            }
        })
    }
   
    const transaction = {
        clientId: req.body.clientId,
        transactionTypeId: req.body.transactionTypeId,
        submissionDate: req.body.submissionDate
    }
    addTransactionHelper(transaction, req, res)
}

exports.getTransactionAPI = async(req,res) => {
    searchTransactions(req).then((transactions) => {
        return res.status(200).send(transactions);
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}


var searchTransactions = (req) =>  {
    let searchCriteria = [];
    let chequeSearch = [];
    if (req.query.id) {
        searchCriteria.push({transactionId: req.query.id})
    }
    if (req.query.transactionTypeId) {
        searchCriteria.push({transactionTypeId: req.query.transactionTypeId})
    }
    if (req.query.clientId) {
        searchCriteria.push({clientId: req.query.clientId})
    }
    if (req.query.submissionDateFrom) {
        searchCriteria.push({
            submissionDate: { 
                [Op.gte] : req.query.submissionDateFrom
            }
        })
    }
    if (req.query.submissionDateTo) {
        searchCriteria.push({
            submissionDate: { 
                [Op.lte] : req.query.submissionDateTo
            }
        })
    }
    if (req.query.chequeNumber) {
        chequeSearch.push({chequeNumber: req.query.chequeNumber})
    }
    if (req.query.bankId) {
        chequeSearch.push({bankId: req.query.bankId})
    }
    if (req.query.chequeSequence) {
        chequeSearch.push({chequeSequence: req.query.chequeSequence})
    }
    return Transaction.findAll({
        where: {
          [Op.and]: searchCriteria
        },
        include: { model: db.cheque, where: { [Op.and]: chequeSearch}, through: {attributes: []} }
    });
}

exports.deleteTransactionAPI = (req,res) => {
    if (!req.query.transactionId) {
        return res.status(400).send({error: msg.noIdProvided})
    }
    return deleteHelper(req.query.transactionId,res);
}


var deleteHelper = (transactionId, res) => {
    return sequelize.transaction(function (t) {
        return chequeTransactionController.findByTransactionId(transactionId, t).then((cts)=> {
            let chequeIds = [];
            let chequeIdsToBeDeleted = [];
            if (cts && cts.length > 0) {
                chequeIds = cts.map((ct) => ct.chequeId);
                return chequeTransactionController.findByChequeId(chequeIds, t).then((chequeTransactions) => {
                    chequeIds.forEach((chequeId) => {
                        let chequeTransactionForThisCheque = chequeTransactions.filter((chequeTransaction) => chequeTransaction.chequeId == chequeId);
                        if (chequeTransactionForThisCheque.length == 1) {
                            chequeIdsToBeDeleted.push(chequeId);
                        }
                    })
                    return chequeController.bulkDestroy(chequeIdsToBeDeleted, t).then((data) => {
                        return destroy(transactionId, t);
                    })
                });
            } else {
                return destroy(transactionId, t);
            }
        })
    }).then((data)=> {
        console.log('>>>>>>>>>>>>>DELETE>>>>>>>>SUCCESS>>>>>>>>>>>>>>>>>>>>>>>>>>', data);
        return res.status(204).send();

    }).catch((error) => {
        console.log('>>>>>>>>>>>>>>>DELETE>>>>>>ERROR>>>>>>>>>>>>>>>>', error);
        return res.status(500).send({error: error});
    })
}