const db = require("../models");
const Cheque = db.cheque;
const msg = require("../utils/msg")
const Op = db.Sequelize.Op
const bankController = require("../controllers/bank.controller");
const chequeStatusController = require("../controllers/chequeStatus.controller");

var create = (cheque, t) => {
    if (t) {
        console.log('transaction t........cheque');
        return Cheque.create(cheque, {transaction: t});
    } else {
        return Cheque.create(cheque);
    }
}

var bulkCreate = (cheques, t) => {
    return Cheque.bulkCreate(cheques, {returning: true, transaction:t});
}

var createOrUpdate = (cheque, t) => {
    return Cheque.findAll({ 
        where: {chequeNumber: cheque.chequeNumber, bankId: cheque.bankId},
        transaction: t
    }).then((chequeFound)=> {
        if (chequeFound && chequeFound.length == 1) {
            console.log('chequeFound.................', chequeFound);
            return Cheque.update(cheque, {
                where: {
                    chequeId: chequeFound[0].chequeId
                },
                transaction: t
            }).then((data) => {
                return findOne(chequeFound[0].chequeId, t);
            });
        } else {
            return create(cheque, t)
        }
    })
}


var findAll = () => {
    return Cheque.findAll();
}


var findOne = (id, t) => {
    if (t) {
        return Cheque.findByPk(id, {transaction: t});
    } else {
        return Cheque.findByPk(id);
    }
}

var destroy = (id) => {
    return Cheque.destroy({where:{chequeId : id}});
}

exports.bulkDestroy = (ids, t) => {
    if (t) {
        return Cheque.destroy({where:{chequeId : ids }, transaction: t})
    } else {
        return Cheque.destroy({where:{chequeId : ids }})
    }
}
    

var updateById = (obj, id, t) => {
    console.log('updateById.................', obj);
    console.log('Id.................', id);
    if (t) {
        return Cheque.update(obj, {
            where: {
                chequeId: id
            },
            transaction: t
        });
    } else {
        return Cheque.update(obj, {
            where: {
                chequeId: id
            },
        });
    }
   
}



var chequeValidation = (cheque, res) => {
    if (!cheque.chequeNumber) {
        return res.status(400).send({error: msg.noChequeNumberProvided})
    }
    if (!cheque.chequeSequence) {
        return res.status(400).send({error: msg.noChequeSequenceProvided})
    }
    if (!cheque.dueDate) {
        return res.status(400).send({error: msg.noPhoneNumberProvided})
    }
    if (!cheque.amount) {
        return res.status(400).send({error: msg.noAmountProvided})
    }
    if (!cheque.fromName) {
        return res.status(400).send({error: msg.noFromNameProvided})
    }
    if (!cheque.toName) {
        return res.status(400).send({error: msg.noToNameProvided})
    }
    if (!cheque.bankId) {
        return res.status(400).send({error: msg.noBankProvided})
    } else {
        bankController.findOne(cheque.bankId).then((data) => {
            if (!data) {
                return res.status(400).send({error: msg.invalidBankId})
            }
        })
    }
    if (cheque.chequeStatusId) {
        chequeStatusController.findOne(cheque.chequeStatusId).then((data) => {
            if (!data) {
                return res.status(400).send({error: msg.invalidChequeStatusId})
            }
        })
    }
    const chequeObj = {
        chequeNumber: cheque.chequeNumber,
        chequeSequence: cheque.chequeSequence,
        dueDate: cheque.dueDate,
        amount: cheque.amount,
        fromName: cheque.fromName,
        toName: cheque.toName,
        comment: cheque.comment ? cheque.comment : '',
        bankId: cheque.bankId,
        chequeStatusId: cheque.chequeStatusId ? cheque.chequeStatusId : 1
    }
    return chequeObj;
}
  
var AddChequeHelper = (cheque,res, t) => {
    const chequeObj = chequeValidation(cheque, res);
    return create(chequeObj, t);
}


exports.addChequeAPI = (req,res) => {
    AddChequeHelper(req.body,res, undefined).then((data)=> {
        return res.status(200).send(data);
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

var addBulkChequesHelper = (chequesArr, res, t) => {
    chequesArr.forEach((cheque)=> {
        chequeValidation(cheque, res)
    });
    return bulkCreate(chequesArr, t);
}

exports.addBulkCheques = (chequesArr, res, t) => {
    chequesToBeAdded= [];
    existingCheques = [];  
    searchCriteria = [];
    chequesArr.forEach((cheque) => {
        searchCriteria.push({ chequeNumber: cheque.chequeNumber, bankId: cheque.bankId})
    })
    return Cheque.findAll({
        where: {
          [Op.or]: searchCriteria
        },
        transaction: t
    }).then((foundCheques) => {
        console.log('FOUND_CHEQUES_LENGTH.........', foundCheques.length)
        console.log('CHEQUES_ARRAY_LENGTH.........', chequesArr.length)
        if (foundCheques.length  == 0) {
            return addBulkChequesHelper(chequesArr,res, t);
        }else if (foundCheques.length  == chequesArr.length) {
            return foundCheques;
        } else if (foundCheques.length < chequesArr.length) {
            for (let i=0;i<chequesArr.length;i++) {
                let filtered = foundCheques.filter((cheque)=> cheque.chequeNumber == chequesArr[i].chequeNumber && cheque.bankId == chequesArr[i].bankId)
                if (filtered && filtered.length == 0) {
                    chequesToBeAdded.push(chequesArr[i])
                } else {
                    existingCheques.push(filtered[0])
                }
            }
            return addBulkChequesHelper(chequesToBeAdded,res, t).then((data)=> {
                existingCheques.push(... data);
                return existingCheques;
            })
        }
    })
}

exports.findCheques = (chequesArr, res, t) => {
    var promisesArr = [];
    chequesArr.forEach((cheque) => {
        promisesArr.push(AddChequeHelper(cheque, res, t))
    })
    return Promise.all(promisesArr);
}

var findCheque = (cheque, t) =>{
    let searchCriteria = [];
    if (cheque.chequeNumber) {
        searchCriteria.push({chequeNumber: cheque.chequeNumber})
    }
    if (cheque.bankId) {
        searchCriteria.push({bankId: cheque.bankId})
    }
    if (cheque.chequeId) {
        searchCriteria.push({chequeId: cheque.chequeId})
    }
    return Cheque.findAll({
        where: {
          [Op.and]: searchCriteria
        },
        transaction: t
    });
}

// var getChequeByIds = (ids) => {
//     return Cheque.findAll({
//         where: {
//           [Op.in]: ids
//         }
//     });
// }

var searchCheque = (req) =>  {
    let searchCriteria = [];
    if (req.query.chequeNumber) {
        searchCriteria.push({chequeNumber: req.query.chequeNumber})
    }
    if (req.query.chequeSequence) {
        searchCriteria.push({chequeSequence: req.query.chequeSequence})
    }
    if (req.query.dueData) {
        searchCriteria.push({dueData: req.query.dueData})
    }
    if (req.query.amount) {
        searchCriteria.push({amount: req.query.amount})
    }
    if (req.query.chequeId) {
        searchCriteria.push({chequeId: req.query.chequeId})
    }
    if (req.query.bankId) {
        searchCriteria.push({bankId: req.query.bankId})
    }
    if (req.query.chequeStatusId) {
        searchCriteria.push({
            chequeStatusId: { 
                [Op.in] : req.query.chequeStatusId.split("-")
            }
        })
        // searchCriteria.push({chequeStatusId: req.query.chequeStatusId})
    }
    if (req.query.dueDate) {
        searchCriteria.push({
            dueDate: { 
                [Op.eq] : req.query.dueDate
            }
        })
    }
    if (req.query.dueDateFrom) {
        searchCriteria.push({
            dueDate: { 
                [Op.gte] : req.query.dueDateFrom
            }
        })
    }
    if (req.query.dueDateTo) {
        searchCriteria.push({
            dueDate: { 
                [Op.lte] : req.query.dueDateTo
            }
        })
    }
    if (req.query.amountFrom) {
        searchCriteria.push({
            amount: { 
                [Op.gte] : req.query.amountFrom
            }
        })
    }
    if (req.query.amountTo) {
        searchCriteria.push({
            amount: { 
                [Op.lte] : req.query.amountTo
            }
        })
    }
    if (req.query.fromName) {
        searchCriteria.push({
            fromName: { 
                [Op.like] : req.query.fromName
            }
        })
    }
    if (req.query.toName) {
        searchCriteria.push({
            toName: { 
                [Op.like] : req.query.toName
            }
        })
    }
    return Cheque.findAll({
        where: {
          [Op.and]: searchCriteria
        }
    });
}


exports.getChequesAPI = (req,res) => {
    getAllChequesAPIHelper(req).then((data) => {
        return res.status(200).send(data);
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

var getAllChequesAPIHelper = (req) => {
    if ( Object.keys(req.query).length === 0) {
        return findAll();
    } else {
        return searchCheque(req);
    }
}



// getChequeByIdAPI = (req,res) => {
//     if (!req.query.chequeId) {
//         return res.status(400).send({error: msg.noIdProvided})
//     }
//     findOne(req.query.id).then((data) => {
//         if (data) {
//             return res.status(200).send(data);
//         } else {
//             return res.status(404).send({message: msg.notFound}); 
//         }
//     }).catch((error)=> {
//         return res.status(500).send({error: error})
//     })
// }

// exports.getClientAPI = (req,res) => {
//     if (!req.query.id) {
//         return getAllClientsAPI(req,res);
//     } else {
//         return getClientByIdAPI(req, res);
//     }
// }

var deleteChequeHelper = (req,res) => {
    if (!req.query.chequeId) {
        return res.status(400).send({error: msg.noIdProvided})
    } else {
        return destroy(req.query.chequeId);
    }
}

exports.deleteChequeAPI = (req,res) => {
    deleteChequeHelper(req,res).then((data)=>{
        return res.status(204).send();
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

exports.updateChequeAPI = (req,res) => {
    if (!req.body.chequeId) {
        return res.status(400).send({error: msg.noIdProvided})
    }
    updateChequeHelper(req,res).then((data)=>{
        return res.status(204).send();
    }).catch((error)=> {
        return res.status(500).send({error: error})
    })
}

var updateChequeHelper = (req,res) => {
    if (req.body.bankId) {
        bankController.findOne(req.body.bankId).then((data) => {
            if (!data) {
                return res.status(400).send({error: msg.invalidBankId})
            }
        })
    }
    if (req.body.bankId) {
        bankController.findOne(req.body.bankId).then((data) => {
            if (!data) {
                return res.status(400).send({error: msg.invalidBankId})
            }
        })
    }
    if (req.body.chequeStatusId) {
        chequeStatusController.findOne(req.body.chequeStatusId).then((data) => {
            if (!data) {
                return res.status(400).send({error: msg.invalidChequeStatusId})
            }
        })
    }
    let cheque = {};
    if(req.body.chequeNumber) {
        cheque.chequeNumber = req.body.chequeNumber
    }
    if(req.body.chequeSequence) {
        cheque.chequeSequence = req.body.chequeSequence
    }
    if(req.body.dueDate) {
        cheque.dueDate = req.body.dueDate
    }
    if(req.body.amount) {
        cheque.amount = req.body.amount
    }
    if(req.body.fromName) {
        cheque.fromName = req.body.fromName
    }
    if(req.body.toName) {
        cheque.toName = req.body.toName
    }
    if(req.body.bankId) {
        cheque.bankId = req.body.bankId
    }
    if(req.body.chequeStatusId) {
        cheque.chequeStatusId = req.body.chequeStatusId
    }
    return updateById(cheque, req.body.chequeId);
    // const cheque = {
    //     chequeNumber: req.body.chequeNumber ? req.body.chequeNumber : delete cheque.chequeNumber,
    //     chequeSequence: req.body.chequeSequence ? req.body.chequeSequence : delete cheque.chequeSequence,
    //     dueDate: req.body.dueDate ? req.body.dueDate : delete cheque.dueDate,
    //     amount: req.body.amount ? req.body.amount,
    //     fromName: req.body.fromName,
    //     toName: req.body.toName,
    //     comment: req.body.comment,
    //     bankId: req.body.bankId,
    //     chequeStatusId: req.body.chequeStatusId ? req.body.chequeStatusId : 1
    // }
   
}

