const db = require("../models");
const ChequeTransaction = db.chequeTransaction;


var findAll = () => {
    return ChequeTransaction.findAll();
}

exports.findByTransactionId = (id, t) => {
    if (t) {
        return ChequeTransaction.findAll({where:{transactionId : id}, transaction:t})
    } else {
        return ChequeTransaction.findAll({where:{transactionId : id}})
    }
}

exports.findByChequeId = (id, t) => {
    if (t) {
        return ChequeTransaction.findAll({where:{chequeId : id}, transaction:t})
    } else {
        return ChequeTransaction.findAll({where:{chequeId : id}})
    }
}

var create =(chequeTransaction, t) => {
    if (t) {
        return ChequeTransaction.create(chequeTransaction, {transaction: t});
    } else {
        return ChequeTransaction.create(chequeTransaction)
    }
}

var addChequeTransaction = (chequeTransaction, res, t) => {
    if (!chequeTransaction.chequeId) {
        return res.status(400).send({error: msg.noChequeNumberProvided})
    }
    if (!chequeTransaction.transactionId) {
        return res.status(400).send({error: msg.noChequeNumberProvided})
    }
    return create(chequeTransaction, t);
}

exports.addBulkChequeTransaction = (chequeTransactionArr, res, t) => {
    var promisesArr = [];
    chequeTransactionArr.forEach((chequeTransaction) => {
        promisesArr.push(addChequeTransaction(chequeTransaction, res, t))
    })
    return Promise.all(promisesArr);
}

