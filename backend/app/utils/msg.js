module.exports = {
    noTokenProvided: 'No token provided.',
    noOperatorProvided: 'No operator Provided.',
    noNameProvided:"MISSING_NAME",
    noTranslationProvided:"MISSING_TRANSLATION",
    noIdProvided:"MISSING_ID",
    noBrandNameProvided:"MISSING_BRAND_NAME",
    noPhoneNumberProvided:"MISSING_PHONE_NUMBER",
    noBankProvided:"MISSING_BANK",
    noFromNameProvided:"MISSING_FROM_NAME",
    noToNameProvided:"MISSING_TO_NAME",
    noChequeNumberProvided:"MISSING_TO_NAME",
    noAmountProvided:"MISSING_AMOUNT",
    noChequeSequenceProvided:"MISSING_CHEQUE_SEQUENCE",
    invalidBankId:"INVALID_BANK",
    invalidClientId:"INVALID_CLIENT",
    invalidTransactionTypeId:"INVALID_TRANSACTION_TYPE",
    invalidChequeStatusId:"INVALID_CHEQUE_STATUS",
    notFound:"NOT_FOUND",
    noChequesProvided:"NO_CHEQUES_PROVIDED",
    notAuthorized: "You don't have enough permission to perform this action.",
    getRoleError: "Error while getting role.",
    getUserError: "Error while getting user.",
    authenticationFailed: "Failed to authenticate token.",
    tokenExpired:"Token is Expired, please login again.",
    notSameTokenInDB: "Token given is not the same token in the database, please login again.",
    tokenBelongToOtherUser: "Error this token doesn't belong to this user.",
    
}

