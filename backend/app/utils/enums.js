
const jwtErrors = { 
    "TokenExpiredError": "TokenExpiredError",
    "JsonWebTokenError":"JsonWebTokenError",
    "NotBeforeError": "NotBeforeError"};
Object.freeze(jwtErrors)


exports.dbTables = { 
    "Cheque": { modelName:"Cheque", resource:"user" },
    "ChequeHistory": { modelName:"ChequeHistory", resource:"user" },
    "Transaction": { modelName:"Transaction", resource:"role" },
    "Client": { modelName:"Client", resource:"userRole"},
    "Bank": { modelName:"Bank", resource:"userRole"},
    "TransactionType": { modelName:"TransactionType", resource:"userRole"},
    "ChequeStatus": { modelName:"ChequeStatus", resource:"userRole"},
    "ChequeTransaction": { modelName:"ChequeTransaction", resource:"userRole"}

};


exports.roleTableApiSecurityRoles = {
    "create": "createAny",
    "getAll": "readAny",
    "getById": "readAny",
    "deleteById": "deleteAny"
}

exports.userTableApiSecurityRoles = {
    "getAll": "readAny",
    "getById": "readOwn",
    "deleteById": "deleteAny"
}

exports.userRoleTableApiSecurityRoles = {
    "create": "createAny",
    "getAll": "readAny",
    "getById": "readOwn",
    "deleteById": "deleteAny",
    "getAllUserRoles": "readOwn",
    "getAllRoleUsers": "readAny"
}
  

