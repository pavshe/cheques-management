module.exports = app => {
    const chequeController = require("../controllers/cheque.controller");
    // var router = require("express").Router();
    var express = require('express');
    var router = express.Router();

    router.post("/", chequeController.addChequeAPI)
    router.get("/", chequeController.getChequesAPI)
    router.delete("/", chequeController.deleteChequeAPI)
    router.put("/", chequeController.updateChequeAPI)
    app.use('/cm/cheque', router);
}

// var express = require('express');
// var router = express.Router();
// const chequeController = require("../controllers/cheque.controller");

// // middleware that is specific to this router
// router.use(function timeLog (req, res, next) {
//     console.log('Time: ', Date.now())
//     console.log('cheque routing???????????????????????????????????');
//     next()
// })

// router.post("/", chequeController.addChequeAPI)
// router.get("/", chequeController.getChequesAPI)
// router.delete("/", chequeController.deleteChequeAPI)
// module.exports = router;
