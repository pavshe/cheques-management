module.exports = app => {
    const clientController = require("../controllers/client.controller");
    // var router = require("express").Router();
    var express = require('express');
    var router = express.Router();

    router.post("/", clientController.addClientAPI)
    router.get("/", clientController.getClientAPI)
    router.delete("/", clientController.deleteClientAPI)
    app.use('/cm/client', router);
}

// var express = require('express');
// var router = express.Router();
// const clientController = require("../controllers/client.controller");

// // middleware that is specific to this router
// router.use(function timeLog (req, res, next) {
//     console.log('Time: ', Date.now())
//     console.log('Client routing.................................');
//     next()
// })
// router.post("/", clientController.addClientAPI)
// router.get("/", clientController.getClientAPI)
// router.delete("/", clientController.deleteClientAPI)
// module.exports = router;
