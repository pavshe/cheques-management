module.exports = app => {
    const bankController = require("../controllers/bank.controller");
    var multer = require( 'multer');
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, 'uploads')
        },
        filename: function (req, file, cb) {
          cb(null, file.fieldname + '-' + Date.now())
        }
    })
       
    var upload = multer(); //multer({ storage: storage })
    // var router = require("express").Router();
    var express = require('express');
    var router = express.Router();

    router.post("/", bankController.addBankAPI)
    router.get("/", bankController.getBankAPI)
    router.delete("/", bankController.deleteBankAPI)
    router.post("/file", upload.single('pavly'), bankController.addBankBulkAPI)
    app.use(upload.single('pavly'));
    app.use('/cm/bank', router);
}

// var express = require('express');
// var router = express.Router();
// const bankController = require("../controllers/bank.controller");

// // middleware that is specific to this router
// router.use(function timeLog (req, res, next) {
//     console.log('Time: ', Date.now())
//     console.log('Bank routing$$$$$$$$$$$$$$$$$$$$$$$$');
//     next()
// })

// router.post("/", bankController.addBankAPI)
// router.get("/", bankController.getBankAPI)
// router.delete("/", bankController.deleteBankAPI)
// module.exports = router;

