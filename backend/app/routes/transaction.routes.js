module.exports = app => {
    const transactionController = require("../controllers/transaction.controller");
    // var router = require("express").Router();
    var express = require('express');
    var router = express.Router();

    router.post("/", transactionController.addTransactionAPI)
    router.get("/", transactionController.getTransactionAPI)
    router.delete("/", transactionController.deleteTransactionAPI)
    app.use('/cm/transaction', router);
}