const enums = require("../utils/enums")

module.exports = (sequelize, Sequelize) => {
    const Transaction = sequelize.define(enums.dbTables.Transaction.modelName, {
      transactionId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      submissionDate: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
      // type: {
      //   type: Sequelize.ENUM('ComingIn', 'GoingOut'),
      //   defaultValue: 'ComingIn',
      //   allowNull: false
      // },
      // clientId: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // }
    });
  
    return Transaction;
  };
