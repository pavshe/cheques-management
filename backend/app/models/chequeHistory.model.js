const enums = require("../utils/enums")

module.exports = (sequelize, Sequelize) => {
    const Cheque = sequelize.define(enums.dbTables.ChequeHistory.modelName, {
      chequeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      chequeNumber: {
        type: Sequelize.STRING,
        allowNull: false
      },
      chequeSequence: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      dueData: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      amount: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      fromName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      toName:{
        type: Sequelize.STRING,
        allowNull: false
      },
      comment:{
        type: Sequelize.STRING,
        allowNull: true
      },
      // bankId: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // },
      // chequeStatusId: {
      //   type: Sequelize.STRING,
      //   allowNull: false,
      //   defaultValue: 'CollectedFromClient'
      // }
    });

    return Cheque;
  };