const enums = require("../utils/enums")

module.exports = (sequelize, Sequelize) => {
    const TransactionType = sequelize.define(enums.dbTables.TransactionType.modelName, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
      }
    });
  
    return TransactionType;
};
// ('ComingIn', 'GoingOut')