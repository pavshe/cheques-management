const enums = require("../utils/enums")

module.exports = (sequelize, Sequelize) => {
    const Bank = sequelize.define(enums.dbTables.Bank.modelName, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
      },
      translation: {
        type: Sequelize.STRING,
        allowNull: false
      }
    });
  
    return Bank;
  };
  