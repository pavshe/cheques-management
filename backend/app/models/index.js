const dbConfig = require("../../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.bank = require("./bank.model")(sequelize, Sequelize);
db.client = require("./client.model")(sequelize, Sequelize);
db.cheque = require("./cheque.model")(sequelize, Sequelize);
db.chequeStatus = require("./cheque.status.model")(sequelize, Sequelize);
db.chequeHistory = require("./chequeHistory.model")(sequelize, Sequelize);
db.transactionType = require("./transaction.type.model")(sequelize, Sequelize);
db.chequeTransaction = require("./cheque.transaction.model")(sequelize, Sequelize);
db.transaction = require("./transaction.model")(sequelize, Sequelize);

// console.log('dbbbbbbb......................................', db.bank)

db.cheque.belongsTo(db.bank, {as: 'bank'});
db.cheque.belongsTo(db.chequeStatus, {as: 'chequeStatus'});
db.chequeHistory.belongsTo(db.bank, {as: 'bank'});
db.chequeHistory.belongsTo(db.chequeStatus, {as: 'chequeStatus'});
db.transaction.belongsTo(db.transactionType, {as: 'transactionType'});
db.transaction.belongsTo(db.client, {as: 'client'});

db.transaction.belongsToMany(db.cheque, {through: db.chequeTransaction, foreignKey: 'transactionId', otherKey: 'chequeId'});
db.cheque.belongsToMany(db.transaction, {through: db.chequeTransaction, foreignKey: 'chequeId', otherKey: 'transactionId'});

module.exports = db;

// db.cheque.hasOne(db.bank, {as: 'bankId'});
// db.cheque.hasOne(db.chequeStatus, {as: 'chequeStatusId'});
// db.chequeHistory.hasOne(db.bank, {as: 'bankId'});
// db.chequeHistory.hasOne(db.chequeStatus, {as: 'chequeStatusId'});
// db.transaction.hasOne(db.transactionType, {as: 'transactionTypeId'});
// db.transaction.hasOne(db.client, {as: 'clientId'});