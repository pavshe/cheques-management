const enums = require("../utils/enums")

module.exports = (sequelize, Sequelize) => {
    const Client = sequelize.define(enums.dbTables.Client.modelName , {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      brandName: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      phoneNumber: {
        type: Sequelize.BIGINT,
        allowNull: false,
      },
      bankAcountNumber: {
        type: Sequelize.STRING,
        allowNull: true
      },
      nationalId: {
        type: Sequelize.STRING,
        allowNull: true
      },
      address: {
        type: Sequelize.STRING,
        allowNull: true
      },
      isSupplier: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      }
    });
  
    return Client;
  };