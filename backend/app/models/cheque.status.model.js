const enums = require("../utils/enums")

module.exports = (sequelize, Sequelize) => {
    const ChequeStatus = sequelize.define(enums.dbTables.ChequeStatus.modelName, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
      }
    });
  
    return ChequeStatus;
  };
  

  /**
   *'CollectedFromClient',
    'ReadyForBankSubmission',
    'SubmittedToBank', 
    'RejectedFromBank',
    'ClearedDebit',
    'BankCollectionSucess',
    'Destroyed'
   */
    