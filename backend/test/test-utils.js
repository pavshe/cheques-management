const names = [
    'Jomana', 'Shery', 'Marten', 'Pavly' ,'Atef', 'Mario', 'Martha', 'Nancy', 'Tharwat', 'Amani',
    'Treza', 'Gamal', 'Marten', 'Mando', 'Mira', 'Peter', 'Mikel', 'Haidy', 'Fayez', 'Markos', 'Joja'
];

const cities = [
    'Cairo', 'Giza', 'Qalyobia', 'Alexandria', 'Luxor', 'Aswan'
];

const states = [
    'New Cairo', 'Misr al Gdida', 'Nasr City', 'Korba', 'New Nozha', 'Zamalek'
];

const streetNames = [
    'Saed abd el wares', 'Dr Youssef Mourad', 'Ahmad mahmoud', 'metwaly el zanaty'
];

const banks = [
    {name: 'HSBC', translation:'HSBC-T'},
    {name: 'CIB', translation:'CIB-T'},
    {name: 'NBE', translation:'NBE-T'},
    {name: 'QNB', translation:'QNB-T'},
    {name: 'NBD', translation:'NBD-T'},
    {name: 'ALEX', translation:'ALEX-T'}
]

const chequeStatues = [
    { "name": "CollectedFromClient" },
    { "name": "ReadyForBankSubmission"},
    { "name": "SubmittedToBank"},
    { "name": "RejectedFromBank"},
    { "name": "ClearedDebit"},
    { "name": "BankCollectionSucess"},
    { "name": "Destroyed"}
];

exports.getChequeStatuses = () => {
    return chequeStatues;
}

var getName = () => {
    const random = getRandomInt(names.length - 1);
    return names[random];
}

var getFullName = () => {
    const randomF= getRandomInt(names.length - 1);
    const randomL = getRandomInt(names.length - 1);
    return names[randomF] + ' ' + names[randomL] ;
}

var getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}

exports.getBank = (index) => {
    if (index) {
        return banks[index];
    } else {
        const random = getRandomInt(banks.length - 1);
        return banks[random];
    }
}

var getAllBanks = () => {
    return banks;
}

var getState = () => {
    const random = getRandomInt(states.length - 1);
    return states[random];
}

var getStreet = () => {
    const random = getRandomInt(streetNames.length - 1);
    return streetNames[random];
}

var getCity = () => {
    const random = getRandomInt(cities.length - 1);
    return cities[random];
}


var getPhoneNumber = () => {
    let random = getRandomInt(9999999999);
    const length = random.toString().length;
    let rest;
    if (length < 10) {
        rest = '0'.repeat(10-length);
        random = random.toString() + rest;
    }
    console.log('getPhoneNumber....', random.toString())
    return '0' + random.toString();
}

var getAddress = () => {
    const address = getRandomInt(20) + ', ' + getStreet() + ', ' + getState() + ', ' + getCity();
    return address;
}

var getAccountNumber = () =>{
    const random = getRandomInt(999999999999999999);
    return random.toString();
}


var getChequeNumber = () => {
    const random = getRandomInt(999999999999999999);
    return random.toString();
}

var getChequeSequence = () => {
    const random = getRandomInt(99999);
    return random;
}

var getDueDate = () => {
    const random = getRandomInt(90);
    var d = new Date();
    d.setDate(d.getDate()-random);
    return d.toISOString().slice(0,10);
}

var getNationalId = () => {
    const random = getRandomInt(999999999999999999);
    return random.toString();
}

var getAmount = () => {
    const random = getRandomInt(999999);
    return random;
}



exports.getClient = (isSupplier) => {
    const client = {
        brandName: getFullName(),
        name: getFullName(),
        phoneNumber: getPhoneNumber(),
        nationalId: getNationalId(),
        address: getAddress(),
        isSupplier: isSupplier,
        bankAcountNumber: getAccountNumber()
    }
    return client;
}

exports.getCheque = (bankId, chequeStatusId, comment) => {
    const cheque = {
        chequeNumber: getChequeNumber(),
        chequeSequence: getChequeSequence(),
        dueDate: getDueDate(),
        amount: getAmount(),
        fromName: getFullName(),
        toName: getFullName(),
        comment: comment ? comment : '',
        bankId: bankId,
        chequeStatusId: chequeStatusId ? chequeStatusId : 1
    }
    return cheque;
}

    

