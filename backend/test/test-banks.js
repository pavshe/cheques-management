process.env.NODE_ENV = 'test';

var expect  = require('chai').expect;
var request = require('request');

// Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server.js');
let should = chai.should();
const db = require("../app/models");
const testUtils = require("./test-utils")
const Bank = db.bank;
const config = require('../config/app.config.js');

chai.use(chaiHttp);
let bankToBeSaved = testUtils.getBank(1);
let bankToBeDeleted = testUtils.getBank(2);
let bankToBeGotById = testUtils.getBank(3);//{name: 'manal', translation:'manalTranslation'}; //


// before(async () => {
//     await db.sequelize.sync({ alter: false, force: true });
// });
// 4926334018940203

describe('GET All banks API test(s)', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
    });
    it('it should GET all the banks', (done) => {
      chai.request(server)
          .get('/cm/bank')
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
            done();
        });
    });
});

// 'http://'+config.publishedHostname+':'+config.publishedPort
describe('POST Bank', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
    });
    it('it should add bank in the database', (done) => {
        console.log('bankToBeSaved %%%%%%%%%%%%%%%%%%%%%get......tests.....?????', bankToBeSaved);
        chai.request(server)
          .post('/cm/bank')
          .send(bankToBeSaved)
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('id');
                res.body.should.have.property('name').eql(bankToBeSaved.name);
            done();
        });
    });
});

describe('get bank by id', () => {
    before(async function(){
        await db.sequelize.sync({ alter: false, force: true });
        bankToBeGotById = await Bank.create(bankToBeGotById);
        console.log('bankToBeGotById %%%%%%%%%%%%%%%%%%%%%get......tests.....?????', bankToBeGotById);
    });
    it('it should search on bank with bank id ', (done) => {
    chai.request(server)
          .get('/cm/bank')
          .query({ id: bankToBeGotById.id})
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name').eql(bankToBeGotById.name);
                res.body.should.have.property('translation').eql(bankToBeGotById.translation);
                res.body.should.have.property('id').eql(bankToBeGotById.id);
            done();
        });
    });
});

describe('bank delete', () => {
    before(async function(){
        await db.sequelize.sync({ alter: false, force: true });
        bankToBeDeleted = await Bank.create(bankToBeDeleted);
        console.log('bankToBeDeleted delete DDD......tests.....?????', bankToBeDeleted);
    });
    it('it should delete on bank with bank id ', (done) => {
    chai.request(server)
          .delete('/cm/bank')
          .query({ id: bankToBeDeleted.id })
          .end((err, res) => {
                res.should.have.status(204);
                res.body.should.be.empty;
            done();
          });
        });
});

