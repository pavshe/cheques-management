process.env.NODE_ENV = 'test';

var expect  = require('chai').expect;
var request = require('request');

// Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server.js');
let should = chai.should();
const db = require("../app/models");
const testUtils = require("./test-utils")
const Client = db.client;
const config = require('../config/app.config.js');

chai.use(chaiHttp);
let clientToBeSaved = testUtils.getClient(true);
let clientToBeDeleted = testUtils.getClient(true);
let clientToBeGotById = testUtils.getClient(true);
let clientToBeUpdated = testUtils.getClient(true);


// before(async () => {
//     await db.sequelize.sync({ alter: false, force: true });
// });
// 4926334018940203

describe('GET All clients API test(s)', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
    });
    it('it should GET all the clients', (done) => {
      chai.request(server)
          .get('/cm/client')
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
            done();
        });
    });
});

// 'http://'+config.publishedHostname+':'+config.publishedPort
describe('POST Client', () => {
    it('it should add client in the database', (done) => {
        console.log('clientToBeSaved %%%%%%%%%%%%%%%%%%%%%get......tests.....?????', clientToBeSaved);
        chai.request(server)
          .post('/cm/client')
          .send(clientToBeSaved)
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('id');
                res.body.should.have.property('name').eql(clientToBeSaved.name);
                res.body.should.have.property('brandName').eql(clientToBeSaved.brandName);
            done();
        });
    });
});

describe('get client by id', () => {
    before(async function(){
        await db.sequelize.sync({ alter: false, force: true });
        clientToBeGotById = await Client.create(clientToBeGotById);
        console.log('clientToBeGotById %%%%%%%%%%%%%%%%%%%%%get......tests.....?????', clientToBeGotById);
    });
    it('it should search on client with client id ', (done) => {
    chai.request(server)
          .get('/cm/client')
          .query({ id: clientToBeGotById.id})
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('name').eql(clientToBeGotById.name);
                res.body.should.have.property('brandName').eql(clientToBeGotById.brandName);
                res.body.should.have.property('id').eql(clientToBeGotById.id);
            done();
        });
    });
});

describe('client delete', () => {
    before(async function(){
        await db.sequelize.sync({ alter: false, force: true });
        clientToBeDeleted = await Client.create(clientToBeDeleted);
        console.log('clientToBeDeleted delete DDD......tests.....?????', clientToBeDeleted);
    });
    it('it should delete on client with client id ', (done) => {
    chai.request(server)
          .delete('/cm/client')
          .query({ id: clientToBeDeleted.id })
          .end((err, res) => {
                res.should.have.status(204);
                res.body.should.be.empty;
            done();
          });
    });
});

