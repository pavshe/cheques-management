process.env.NODE_ENV = 'test';

var expect  = require('chai').expect;
var request = require('request');

// Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server.js');
let should = chai.should();
const db = require("../app/models");
const testUtils = require("./test-utils")
const Bank = db.bank;
const Cheque = db.cheque;
const ChequeStatus = db.chequeStatus;
const config = require('../config/app.config.js');
const sequelize_fixtures = require('sequelize-fixtures');

chai.use(chaiHttp);
let chequeToBePosted;
let savedBank;
let savedCheque;
var bankObj = testUtils.getBank(0);

// 4926334018940203

describe('GET All cheques API test(s)', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
        sequelize_fixtures.loadFile('./app/seeders/*.js', db).then(() =>{
            console.log("database is updated!");
        });
    });
    it('it should GET all the cheques', (done) => {
      chai.request(server)
          .get('/cm/cheque')
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
            done();
          });
    });
});

// 'http://'+config.publishedHostname+':'+config.publishedPort
describe('POST Cheque', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
        sequelize_fixtures.loadFile('./app/seeders/*.js', db).then(() =>{
            console.log("database is updated!");
        });
        var bankObj = testUtils.getBank(0);
        savedBank = await Bank.create(bankObj);
        chequeToBePosted = testUtils.getCheque(savedBank.id);
    });
    it('it should add cheque in the database', (done) => {
        chai.request(server)
          .post('/cm/cheque')
          .send(chequeToBePosted)
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('chequeId');
                res.body.should.have.property('bankId').eql(chequeToBePosted.bankId);
            done();
          });
    });
});

describe('cheque search', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
        sequelize_fixtures.loadFile('./app/seeders/*.js', db).then(() =>{
            console.log("database is updated!");
        });
        var bankObj = testUtils.getBank(0);
        savedBank = await Bank.create(bankObj);
        savedCheque = await Cheque.create(testUtils.getCheque(savedBank.id));
    });
    it('it should search on cheque with cheque number ', (done) => {
    chai.request(server)
          .get('/cm/cheque')
          .query({ chequeNumber: savedCheque.chequeNumber, bankId: savedCheque.bankId })
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(1);
            done();
          });
        });
});

describe('cheque delete', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
        sequelize_fixtures.loadFile('./app/seeders/*.js', db).then(() =>{
            console.log("database is updated!");
        });
        var bankObj = testUtils.getBank(0);
        savedBank = await Bank.create(bankObj);
        savedCheque = await Cheque.create(testUtils.getCheque(savedBank.id));
    });
    it('it should delete on cheque with cheque id ', (done) => {
    chai.request(server)
          .delete('/cm/cheque')
          .query({ chequeId: savedCheque.chequeId })
          .end((err, res) => {
                res.should.have.status(204);
                res.body.should.be.empty;
            done();
          });
        });
});

describe('cheque update', () => {
    before(async () => {
        await db.sequelize.sync({ alter: false, force: true });
        sequelize_fixtures.loadFile('./app/seeders/*.js', db).then(() =>{
            console.log("database is updated!");
        });
        var bankObj = testUtils.getBank(0);
        savedBank = await Bank.create(bankObj);
        savedCheque = await Cheque.create(testUtils.getCheque(savedBank.id));
    });
    it('it should update the cheque', (done) => {
      chai.request(server)
          .put('/cm/cheque')
          .send({chequeId: savedCheque.chequeId, chequeSequence: 2222})
          .end((err, res) => {
                res.should.have.status(204);
                res.body.should.be.empty;
            done();
          });
    });
});
