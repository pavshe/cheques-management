const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require('helmet');
const morgan = require('morgan');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
const config = require('./config/app.config.js');
process.env.NODE_ENV = config.enviroment;
const sequelize_fixtures = require('sequelize-fixtures');
var multer = require( 'multer');
var upload = multer();

// const jwt = require('express-jwt');
// const jwksRsa = require('jwks-rsa');

const app = express();

var corsOptions = {
  origin: "http://"+config.publishedHostname+":8081"
};

var dev = true;

app.use(cors(corsOptions));

// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// For multi form data
// app.use(upload.single());

// adding Helmet to enhance your API's security
app.use(helmet());

// adding morgan to log HTTP requests
app.use(morgan('combined'));


// const jwtCheck = jwt({
//   secret: jwksRsa.expressJwtSecret({
//       cache: true,
//       rateLimit: true,
//       jwksRequestsPerMinute: 5,
//       jwksUri: 'https://dev-x334jfhq.auth0.com/.well-known/jwks.json'
// }),
// audience: 'https://login-api',
// issuer: 'https://dev-x334jfhq.auth0.com/',
// algorithms: ['RS256']
// });

// app.use(jwtCheck);


// simple route
// app.get("/", (req, res) => {
//   res.json({ message: "Welcome to bezkoder application." });
// });

const db = require("./app/models");

if (config.enviroment === 'dev') {
  console.log('DDDDDEEEEBBBBUUUUGGGG....................,,,,:::::<<<::,:<:<;');
  db.sequelize.sync({ alter: false, force: true }).then(() => {
    console.log("Drop and re-sync db.");
    sequelize_fixtures.loadFile('./app/seeders/*.js', db).then(() =>{
      console.log("database is updated!");
    });
  }).catch((error) => {
    console.log("error in DB updated!", error);
  });
} else if (config.enviroment === 'prod'){
  db.sequelize.sync();
}


// var bankRoutes = require('./app/routes/bank.routes');
// var clientRoutes = require('./app/routes/client.routes');
// var chequeRoutes = require('./app/routes/cheque.routes');
// app.use('/cm/bank', bankRoutes);
// app.use('/cm/cheque', chequeRoutes);
// app.use('/cm/client', clientRoutes);

// app.use('/cm/bank', require("./app/routes/bank.routes"));
// app.use('/cm/client', require("./app/routes/client.routes"));
// app.use('/cm/cheque', require("./app/routes/cheque.routes"));

require("./app/routes/bank.routes")(app);
require("./app/routes/client.routes")(app);
require("./app/routes/cheque.routes")(app);
require("./app/routes/transaction.routes")(app);


// set port, listen for requests
const PORT = process.env.PORT || config.publishedPort;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

module.exports = app;
